//
//  userData.swift
//  PharmaCompass
//
//  Created by Mohamed Nawar on 1/23/20.
//  Copyright © 2020 Mohamed Nawar. All rights reserved.
//
import Foundation
struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    var data : UserDetails?
    var token : String?
    var identifier : Int?
    var firstUse:String?
   
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "identifier")

        token = ""
    }
    
    mutating func removeIdentifierInside() {
        UserDefaults.standard.removeObject(forKey: "identifier")
    }
   
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "identifier") {
            self.identifier = Int(data)
        }
        if let  data = UserDefaults.standard.string(forKey: "firstUse") {
            self.firstUse = String(data)
        }
    }
}
struct UserDetails : Decodable {
    var id:Int?
    var unseen_coversations_count:Int?
    var name : String?
    var email:String?
    var mobile:String?
    var type:String?
    var description:String?
    var stage: String?
    var rate_student: Int?
    var preference_student: String?
    var about: String?
    var avatar: String?
    var is_connected: Bool?
}
