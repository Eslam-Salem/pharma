//
//  model.swift
//  PharmaCompass
//
//  Created by Omar Adel on 2/12/20.
//  Copyright © 2020 Mohamed Nawar. All rights reserved.
//

import Foundation

struct Subject: Decodable {
    var name : String?
    var codes : [code]?
    var chapters : [Chapter]?
}
struct code: Decodable {
    var name: String?
    var note: String?
    var viewsCount: Int?
}
struct Chapter: Decodable {
    var lessons: [Lesson]?
}
struct Lesson: Decodable {
    var name: String?
    var url : String
}
