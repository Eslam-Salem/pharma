//
//  Component.swift
//  PharmaCompass
//
//  Created by Mohamed Nawar on 1/23/20.
//  Copyright © 2020 Mohamed Nawar. All rights reserved.
//

import Foundation
struct Subjects: Decodable {
    var data: [SubjectData]?
}
struct SubjectData: Decodable {
    var id: Int?
    var name: String?
    var avatar: String?
    var newly_lessons_count: Int?
}
