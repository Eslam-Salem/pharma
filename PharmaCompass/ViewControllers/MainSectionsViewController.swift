//
//  MainSectionsViewController.swift
//  PharmaCompass
//
//  Created by Mohamed Nawar on 1/23/20.
//  Copyright © 2020 Mohamed Nawar. All rights reserved.
//

import UIKit
import FirebaseFirestore


class MainSectionsViewController: CustomBaseVC , UICollectionViewDataSource , UICollectionViewDelegate  {
    
    let db = Firestore.firestore()
    var nameColor =   [#colorLiteral(red: 0.4220864773, green: 0.452195406, blue: 0.58906883, alpha: 1),#colorLiteral(red: 0.9289425015, green: 0.566274941, blue: 0.2606683373, alpha: 1),#colorLiteral(red: 0.4862077236, green: 0.8091998696, blue: 0.79018718, alpha: 1),#colorLiteral(red: 0.6546185613, green: 0.6628188491, blue: 0.662755847, alpha: 1),#colorLiteral(red: 0.7069630623, green: 0.6857301593, blue: 0.4331979752, alpha: 1),#colorLiteral(red: 0.9829441905, green: 0.6422890425, blue: 0.7892381549, alpha: 1),#colorLiteral(red: 0.6688814759, green: 0.581382513, blue: 0.4969726205, alpha: 1),#colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 0.5262735445),#colorLiteral(red: 0.4282038808, green: 0.7693952918, blue: 0.973567903, alpha: 1),#colorLiteral(red: 0.5512533784, green: 0.7528917193, blue: 0.5331814885, alpha: 1),#colorLiteral(red: 0.6787017584, green: 0.4313169122, blue: 0.700019598, alpha: 1),#colorLiteral(red: 0.9846814275, green: 0.5291161537, blue: 0.5994984508, alpha: 1)]
    var imagesArray = [#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img44")]
    var nameArray = [NSLocalizedString( "Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: ""),
                     NSLocalizedString("Subject", comment: "")]
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
        
        db.collection("courses").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var index = 0
                for document in querySnapshot!.documents {
// add chapters into static var courses
                    if let stamp = document.get("chapters") {
                        do {
                            if let data = try? JSONSerialization.data(withJSONObject: stamp, options: []) {
                                let parsedChapters = try JSONDecoder().decode([Chapter].self, from: data)
                                Courses.courses?[index].chapters? += parsedChapters
                            }
                            
                        }catch {
                            print ("")
                            print(error)
                        }
                    }
// add codes into static var courses
                    if let stamp = document.get("chapters") {
                        do {
                            if let data = try? JSONSerialization.data(withJSONObject: stamp, options: []) {
                                let parsedCodes = try JSONDecoder().decode([code].self, from: data)
                                Courses.courses?[index].codes! += parsedCodes
                            }
                            
                        }catch {
                            print ("")
                            print(error)
                        }
                    }
// add name of course  into static var courses
                    if let stamp = document.get("name") {
                        Courses.courses?[index].name = stamp as! String
                    }
                }
            }
        }
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let navVC = storyboard?.instantiateViewController(withIdentifier: "SectionFilesViewController") as! SectionFilesViewController
//        navVC.title = NSLocalizedString( "Subject", comment: "")
//        self.navigationController?.pushViewController(navVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainSectionsCell", for: indexPath) 
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
        image.image = imagesArray[indexPath.row]
        lbl.text = nameArray[indexPath.row]
        lbl.textColor = nameColor[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var sectionHeaderView = UICollectionReusableView()
        if collectionView == self.CategoryCollectionView {
            switch kind {
                
            case UICollectionView.elementKindSectionHeader:
                sectionHeaderView = CategoryCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MainSectionsHeader", for: indexPath)
                let img = sectionHeaderView.viewWithTag(3) as! UIImageView
                
                
                img.image = #imageLiteral(resourceName: "618691-L1JHL-1579793857-5e29bdc1cfbeb")
                
                return sectionHeaderView
                
            default:
                assert(false, "Unexpected element kind")
            }
        }
        return sectionHeaderView
    }
}
extension MainSectionsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.CategoryCollectionView.frame.width / 2)
        let height = width - (width/6)
        return CGSize(width: width , height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}



class Courses {
    static var courses : [Subject]?
}


