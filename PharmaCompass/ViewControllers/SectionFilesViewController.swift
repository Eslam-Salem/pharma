//
//  SectionFilesViewController.swift
//  PharmaCompass
//
//  Created by Mohamed Nawar on 1/23/20.
//  Copyright © 2020 Mohamed Nawar. All rights reserved.
//

import UIKit



    class SectionFilesViewController: CustomBaseVC , UITableViewDelegate , UITableViewDataSource  {
        @IBOutlet weak var tableView: UITableView!
  
        override func viewDidLoad() {
            super.viewDidLoad()
            tableView.separatorStyle = .none
            tableView.delegate = self
            tableView.dataSource = self
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
        
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDaysCell", for: indexPath)
                let title = cell.viewWithTag(1) as! UILabel
            title.text = "Subject File"
            return cell
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                let navVC = storyboard?.instantiateViewController(withIdentifier: "SectionContentViewController") as! SectionContentViewController
                navVC.title = NSLocalizedString("SectionContentViewController", comment: "")
              self.navigationController?.pushViewController(navVC, animated: true)


        }
    }
